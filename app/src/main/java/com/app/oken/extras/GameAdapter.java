package com.app.oken.extras;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.app.oken.noleggiovideogiochireal.R;
import com.bumptech.glide.Glide;
import java.util.ArrayList;
import java.util.List;

public class GameAdapter extends RecyclerView.Adapter<GameAdapter.GameViewHolder>{
    private ArrayList<Game> giochi;
    private List<Game> gameUploadList;
    private Context context;
    private OnItemClickListener listener;

    public interface OnItemClickListener{
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener){
        this.listener = listener;
    }

    public class GameViewHolder extends RecyclerView.ViewHolder{
        public ImageView img;
        public TextView txt1, txt2;

        public GameViewHolder(View itemView){
            super(itemView);
            txt1 = itemView.findViewById(R.id.textView1);
            txt2 = itemView.findViewById(R.id.textView2);
            img = itemView.findViewById(R.id.imageView);

            itemView.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view){
                    if(listener != null){
                        int position = getAdapterPosition();
                        if(position != RecyclerView.NO_POSITION){
                            listener.onItemClick(position);
                        }
                    }
                }
            });
        }
    }

    public GameAdapter(Context context, List<Game> gameUploadList){
        this.context = context;
        this.gameUploadList = gameUploadList;
        this.giochi = (ArrayList<Game>) gameUploadList;
    }

    @Override
    public GameViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.games_items, parent, false);
        GameViewHolder gvh = new GameViewHolder(v);
        return gvh;
    }
    @Override
    public void onBindViewHolder(GameViewHolder holder, int position){
        Game game = gameUploadList.get(position);
        holder.txt1.setText(game.getTitolo());
        holder.txt2.setText(game.getGenre());
        Glide.with(context).load(game.getUrl()).into(holder.img);
    }
    @Override
    public int getItemCount(){
        return gameUploadList.size();
    }

    public void setFilter(ArrayList<Game> tempGame){
        gameUploadList = new ArrayList<>();
        gameUploadList.addAll(tempGame);
        notifyDataSetChanged();
    }
}