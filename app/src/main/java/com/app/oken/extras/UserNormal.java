package com.app.oken.extras;

public class UserNormal extends User{
    public UserNormal(){}

    public UserNormal(String nome, String password, String email) {
        super(nome, password, email);
    }
    @Override
    public String toString() {
        return "UserId: " + getNome() + "email: " + getEmail();
    }
}