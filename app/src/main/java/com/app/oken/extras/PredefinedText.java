package com.app.oken.extras;

public class PredefinedText{
    public String databases[];
    public String email;
    public String password;
    public String id;
    public static final String EXTRA_URL = "imageURL";
    public static final String EXTRA_TITOLO = "titolo";
    public static final String EXTRA_GENRE = "genre";

    public PredefinedText(){
        databases = new String[]{"Utenti", "Noleggio", "Giochi", "Normali", "Admin", "Super Admin"};
        id = "super_admin";
        email = "super-admin@gmail.com";
        password = "super-admin";
    }
}