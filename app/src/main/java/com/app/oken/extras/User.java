package com.app.oken.extras;

import java.io.Serializable;

public abstract class User{
    private String nome, cognome, password, email;

    public User(){}
    
    public User(String nome, String password, String email){
        this.nome = nome;
        this.password = password;
        this.email = email;
    }
    public String getNome(){
        return this.nome;
    }
    public String getCognome(){
        return this.cognome;
    }
    public String getPassword(){
        return this.password;
    }
    public String getEmail(){
        return this.email;
    }
    public void setNome(String nome){
        this.nome = nome;
    }
    public void setCognome(String password) {
        this.password = password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public void setEmail(String email){
        this.email = email;
    }

    @Override
    public String toString(){
        return "User{" +
                "nome='" + nome + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}