package com.app.oken.extras;

public class Noleggio{
    String id, costo, date, titolo;

    public Noleggio(){}

    public Noleggio(String titolo, String date, String costo){
        this.titolo = titolo;
        this.date = date;
        this.costo = costo;
    }
    public String getDate(){
        return date;
    }
    public void setDate(String date){
        this.date = date;
    }
    public String getCosto(){
        return costo;
    }
    public void setCosto(String costo){
        this.costo = costo;
    }
    public String getTitolo(){
        return this.titolo;
    }


    @Override
    public String toString(){
        return " Titolo = " + titolo + "\n"
                 + " Costo = " + costo + "\n"
                 + " Data   = " + date;
    }
}