package com.app.oken.extras;

public class Game{
    public String titolo, genre, url, date;

    public Game(){}

    public Game(String titolo, String genre, String url, String date){
        this.titolo = titolo;
        this.genre = genre;
        this.url = url;
        this.date = date;
    }
    public String getTitolo(){
        return this.titolo;
    }
    public String getUrl(){
        return this.url;
    }
    public String getGenre(){
        return this.genre;
    }
    public String getDate(){
        return this.date;
    }
    public void setTitolo(String titolo){
        this.titolo = titolo;
    }
    public void setGenre(String genre){
        this.genre = genre;
    }

    @Override
    public String toString() {
        return this.titolo;
    }
}