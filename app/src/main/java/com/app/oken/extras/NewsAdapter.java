package com.app.oken.extras;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.app.oken.noleggiovideogiochireal.R;
import com.bumptech.glide.Glide;

import java.util.List;

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.NewsViewHolder>{
    private List<Game> newsUploadList;
    private List<String> newsImages;
    private Context context;

    public class NewsViewHolder extends RecyclerView.ViewHolder{
        public ImageView img;

        public NewsViewHolder(View itemView){
            super(itemView);
            //img = itemView.findViewById(R.id.imgNews);
        }
    }

    public NewsAdapter(Context context, List<String> newsImages){
        this.context = context;
        //this.newsUploadList = newsUploadList;
        this.newsImages = newsImages;
    }
    @Override
    public NewsViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.news_items, parent, false);
        NewsAdapter.NewsViewHolder gvh = new NewsAdapter.NewsViewHolder(v);
        return gvh;
    }
    @Override
    public void onBindViewHolder(NewsAdapter.NewsViewHolder holder, int position){
        Game game = newsUploadList.get(position);
        Glide.with(context).load(game.getUrl()).into(holder.img);
    }
    @Override
    public int getItemCount(){
        return newsUploadList.size();
    }
}