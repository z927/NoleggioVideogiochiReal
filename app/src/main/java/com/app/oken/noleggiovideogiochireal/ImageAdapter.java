package com.app.oken.noleggiovideogiochireal;

import android.content.Context;
import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.app.oken.extras.Game;
import com.bumptech.glide.Glide;
import java.util.List;

public class ImageAdapter extends PagerAdapter{
    public Context mContext;
    String[] imgUrls;
    Intent intent;
    List<Game> gameList;

    public ImageAdapter(Context mContext, String[] imgUrls, List<Game> gameList){
        this.mContext = mContext;
        this.imgUrls = imgUrls;
        this.gameList = gameList;
    }

    @Override
    public int getCount(){
        return imgUrls.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object){
        return view == object;
    }
    
    @Override
    public Object instantiateItem(ViewGroup container, final int position){
        ImageView imageView = new ImageView(mContext);
        Glide.with(mContext).
                load(imgUrls[position]).
                placeholder(R.drawable.ic_autorenew_black_24dp).
                error(R.drawable.ic_error_black_24dp).
                into(imageView);

        imageView.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                intent = new Intent(mContext, DetailActivity.class);
                if(position == 0){
                    String url = NewsFragment.urls[position];
                    NewsFragment.superFilter(url);
                }else if(position == 1){
                    String url = NewsFragment.urls[position];
                    NewsFragment.superFilter(url);
                }else{
                    String url = NewsFragment.urls[position];
                    NewsFragment.superFilter(url);
                }
            }
        });
        container.addView(imageView, 0);
        return imageView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((ImageView) object);
    }
}