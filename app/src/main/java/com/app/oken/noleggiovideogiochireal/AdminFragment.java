package com.app.oken.noleggiovideogiochireal;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.app.oken.extras.PredefinedText;
import com.app.oken.extras.UserAdmin;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;


public class AdminFragment extends Fragment{
    ListView listView;
    DatabaseReference databaseReference;
    PredefinedText pt;
    private List<UserAdmin> userList;
    private CustomAdapter customAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_admin, null);
        listView = rootView.findViewById(R.id.listAdmin);
        pt = new PredefinedText();
        databaseReference = FirebaseDatabase.getInstance().getReference(pt.databases[0]);
        userList = new ArrayList<>();
        riempiLista();
        return rootView;
    }

    public void riempiLista(){
        databaseReference.child(pt.databases[4]).addValueEventListener(new ValueEventListener(){
            @Override
            public void onDataChange(DataSnapshot dataSnapshot){
                userList.clear();
                if(dataSnapshot.exists()){
                    for(DataSnapshot snapshot : dataSnapshot.getChildren()){
                        UserAdmin user = snapshot.getValue(UserAdmin.class);
                        userList.add(user);
                    }
                    customAdapter = new CustomAdapter(getActivity(), userList);
                    listView.setAdapter(customAdapter);
                }else{
                    Toast.makeText(getActivity(), "Nessun valore trovato", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError){
                databaseError.getMessage();
            }
        });
    }

    class CustomAdapter extends BaseAdapter {
        List<UserAdmin> userList;
        Context mContext;

        public CustomAdapter(Context mContext, List<UserAdmin> userList){
            this.mContext = mContext;
            this.userList = userList;
        }
        @Override
        public int getCount(){
            return userList.size();
        }
        @Override
        public Object getItem(int i){
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View view, ViewGroup viewGroup){
            view = getLayoutInflater().inflate(R.layout.custom_user_admin_layout, null);
            UserAdmin userAdmin = userList.get(position);

            TextView txtId = view.findViewById(R.id.txtIdAdmin);
            TextView txtMail = view.findViewById(R.id.txtMailAdmin);

            txtId.setText("ID: " + userAdmin.getNome());
            txtMail.setText("Email: " + userAdmin.getEmail());
            return view;
        }
    }
}