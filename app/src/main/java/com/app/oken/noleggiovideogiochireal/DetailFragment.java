package com.app.oken.noleggiovideogiochireal;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.app.oken.extras.Noleggio;
import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import static com.app.oken.extras.PredefinedText.EXTRA_GENRE;
import static com.app.oken.extras.PredefinedText.EXTRA_TITOLO;
import static com.app.oken.extras.PredefinedText.EXTRA_URL;

public class DetailFragment extends Fragment{
    private Button btnNoleggia;
    public String titolo, genre, imageUrl;
    public DatabaseReference databaseNoleggio;
    public FirebaseUser user;
    private DateFormat dateFormat;
    public static final String DATABASE_PATH = "Noleggio";
    private ArrayList<String> noleggioList;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View rootView = inflater.inflate(R.layout.fragment_detail, container, false);

        ImageView imgGame = rootView.findViewById(R.id.imgGame);
        TextView txtTitolo = rootView.findViewById(R.id.txtTitle);
        TextView txtGenre = rootView.findViewById(R.id.txtGenere);

        imageUrl = getArguments().getString(EXTRA_URL);
        titolo = getArguments().getString(EXTRA_TITOLO);
        genre = getArguments().getString(EXTRA_GENRE);

        Glide.with(this).load(imageUrl).placeholder(R.drawable.ic_loading).into(imgGame);
        txtTitolo.setText(titolo);
        txtGenre.setText(genre);

        noleggioList = new ArrayList<>();

        dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

        databaseNoleggio = FirebaseDatabase.getInstance().getReference(DATABASE_PATH + "/" + MainActivity.id);

        btnNoleggia = rootView.findViewById(R.id.btnNoleggia);
        btnNoleggia.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                noleggia();
            }
        });

        return rootView;
    }
    public void noleggia(){
        riempiLista();
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setMessage("Vuoi noleggiare il seguente gioco?");
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton("Si", new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialogInterface, int i){
                if(noleggioList.contains(titolo)){
                    Toast.makeText(getActivity(),"siamo spiacenti, il gioco da voi selezionato, è già in vostro possesso", Toast.LENGTH_SHORT).show();
                }else{
                    Noleggio noleggio = new Noleggio(titolo, dateFormat.format(new Date()).toString(), "15,99 €");
                    databaseNoleggio.child(titolo).setValue(noleggio);
                    Toast.makeText(getActivity(),"Il seguente gioco " + noleggio.getTitolo() + " è stato correttamente noleggiato", Toast.LENGTH_SHORT).show();
                }
            }
        });
        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialogInterface, int i){

            }
        });
        alertDialog.create().show();
    }
    public void riempiLista(){
        databaseNoleggio.addValueEventListener(new ValueEventListener(){
            @Override
            public void onDataChange(DataSnapshot dataSnapshot){
                noleggioList.clear();
                if(dataSnapshot.exists()){
                    for(DataSnapshot snapshot : dataSnapshot.getChildren()){
                        Noleggio noleggio = snapshot.getValue(Noleggio.class);
                        String titolo = noleggio.getTitolo();
                        noleggioList.add(titolo);
                    }
                }else{
                    Toast.makeText(getActivity(), "Nessun valore trovato", Toast.LENGTH_SHORT).show();
                    Noleggio noleggio = new Noleggio(titolo, dateFormat.format(new Date()).toString(), "15,99 €");
                    databaseNoleggio.child(titolo).setValue(noleggio);
                    Toast.makeText(getActivity(),"Il seguente gioco " + noleggio.getTitolo() + " è stato correttamente noleggiato", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError){
                databaseError.getMessage();
            }
        });
    }
}