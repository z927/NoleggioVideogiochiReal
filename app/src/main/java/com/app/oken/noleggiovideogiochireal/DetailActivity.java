package com.app.oken.noleggiovideogiochireal;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.app.oken.extras.Noleggio;
import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import static com.app.oken.extras.PredefinedText.EXTRA_GENRE;
import static com.app.oken.extras.PredefinedText.EXTRA_TITOLO;
import static com.app.oken.extras.PredefinedText.EXTRA_URL;

public class DetailActivity extends AppCompatActivity{
    private Button btnNoleggia;
    public String titolo;
    public DatabaseReference databaseNoleggio;
    public FirebaseUser user;
    private DateFormat dateFormat;
    public static final String DATABASE_PATH = "Noleggio";
    private ArrayList<String> noleggioList;
    private Toolbar customToolbar;

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        if(item.getItemId() == android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        customToolbar = findViewById(R.id.toolbarSearch);
        setSupportActionBar(customToolbar);
        getSupportActionBar().setTitle(R.string.app_name);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        Intent intent = getIntent();
        String imageUrl = intent.getStringExtra(EXTRA_URL);
        titolo = intent.getStringExtra(EXTRA_TITOLO);
        String genre = intent.getStringExtra(EXTRA_GENRE);

        ImageView imgGame = findViewById(R.id.imgGame);
        TextView txtTitolo = findViewById(R.id.txtTitle);
        TextView txtGenre = findViewById(R.id.txtGenere);

        Glide.with(this).load(imageUrl).placeholder(R.drawable.ic_loading).into(imgGame);
        txtTitolo.setText(titolo);
        txtGenre.setText(genre);

        noleggioList = new ArrayList<>();

        dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

        databaseNoleggio = FirebaseDatabase.getInstance().getReference(DATABASE_PATH + "/" + MainActivity.id);

        btnNoleggia = findViewById(R.id.btnNoleggia);
        btnNoleggia.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                noleggia();
            }
        });
    }
    public void noleggia(){
        riempiLista();
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setMessage("Vuoi noleggiare il seguente gioco?");
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton("Si", new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialogInterface, int i){
                Log.e("Tag", Arrays.toString(noleggioList.toArray()));

                if(noleggioList.contains(titolo)){
                    Toast.makeText(getApplicationContext(),"siamo spiacenti, il gioco da voi selezionato, è già in vostro possesso", Toast.LENGTH_SHORT).show();
                }else{
                    Noleggio noleggio = new Noleggio(titolo, dateFormat.format(new Date()).toString(), "15,99 €");
                    databaseNoleggio.child(titolo).setValue(noleggio);
                    Toast.makeText(getApplicationContext(),"Il seguente gioco " + noleggio.getTitolo() + " è stato correttamente noleggiato", Toast.LENGTH_SHORT).show();
                }
            }
        });
        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialogInterface, int i){

            }
        });
        alertDialog.create().show();
    }
    public void riempiLista(){
        databaseNoleggio.addValueEventListener(new ValueEventListener(){
            @Override
            public void onDataChange(DataSnapshot dataSnapshot){
                noleggioList.clear();
                if(dataSnapshot.exists()){
                    for(DataSnapshot snapshot : dataSnapshot.getChildren()){
                        Noleggio noleggio = snapshot.getValue(Noleggio.class);
                        String titolo = noleggio.getTitolo();
                        noleggioList.add(titolo);
                    }
                }else{
                    Toast.makeText(getApplicationContext(), "Nessun valore trovato", Toast.LENGTH_SHORT).show();
                    Noleggio noleggio = new Noleggio(titolo, dateFormat.format(new Date()).toString(), "15,99 €");
                    databaseNoleggio.child(titolo).setValue(noleggio);
                    Toast.makeText(getApplicationContext(),"Il seguente gioco " + noleggio.getTitolo() + " è stato correttamente noleggiato", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError){
                databaseError.getMessage();
            }
        });
    }
}