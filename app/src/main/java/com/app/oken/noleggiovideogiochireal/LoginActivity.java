package com.app.oken.noleggiovideogiochireal;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.app.oken.extras.PredefinedText;
import com.app.oken.extras.SuperAdmin;
import com.app.oken.extras.User;
import com.app.oken.extras.UserNormal;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import java.util.ArrayList;
import java.util.List;

public class LoginActivity extends AppCompatActivity{
    private EditText txtUserID, txtPassword;
    private Button btnSign, btnLogin;
    public Intent intent;
    private TextInputLayout tilUserId;
    private TextInputLayout tilPassword;
    private ProgressDialog progressDialog;
    private FirebaseAuth auth;
    private String email;
    private String password;
    private DatabaseReference databaseReference, databaseUsers;
    private PredefinedText pt;
    private FirebaseUser fu;
    private List<String> userNormalList;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        txtUserID = findViewById(R.id.txtUserID);
        txtPassword = findViewById(R.id.txtPassword);
        btnLogin = findViewById(R.id.btnLogin);
        btnSign = findViewById(R.id.btnSign);
        tilUserId =  findViewById(R.id.sign_input_userID);
        tilPassword = findViewById(R.id.sign_input_password);

        userNormalList = new ArrayList<>();

        auth = FirebaseAuth.getInstance();
        fu = auth.getCurrentUser();

        pt = new PredefinedText();

        databaseUsers = FirebaseDatabase.getInstance().getReference(pt.databases[0]);

        readData();
        initSuperAdmin();

        //Actions
        btnSign.setOnClickListener(new View.OnClickListener() {
            //@Override
            public void onClick(View view) {
                intent = new Intent(LoginActivity.this, SignActivity.class);
                startActivity(intent);
            }
        });
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkLogin();
            }
        });
        progressDialog = new ProgressDialog(this);
    }
    public void checkLogin(){
        email = txtUserID.getText().toString();
        password = txtPassword.getText().toString();

        if(TextUtils.isEmpty(email) || TextUtils.isEmpty(password)){
            tilUserId.setError("I campi non possono essere vuoti");
            tilPassword.setError("I campi non possono essere vuoti");
        }else if(!isValidEmail(email)){
            tilUserId.setError("Si prega di inserire una email corretta");
        }else{
            progressDialog.setMessage("Noleggio Videogiochi");
            progressDialog.setCancelable(false);
            progressDialog.show();

            auth.signInWithEmailAndPassword(email, password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>(){
                @Override
                public void onComplete(@NonNull Task<AuthResult> task){
                    if(task.isSuccessful()){

                        progressDialog.dismiss();
                        finish();

                        if(fu.getUid().equals("MWdm6QgeF0OD1RzwaiIfKb6J5P33")){ //SuperAdmin Case
                            startActivity(new Intent(LoginActivity.this, SuperAdminActivity.class));
                        }else{
                            checkEmail(email);
                        }
                    }else{
                        Toast.makeText(getApplicationContext(),"Credenziali errate",Toast.LENGTH_LONG).show();
                        progressDialog.dismiss();
                        txtUserID.setText("");
                        txtPassword.setText("");
                    }
                }
            });
        }
    }

    public static boolean isValidEmail(CharSequence target){
        if(target == null){
            return false;
        }else{
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    public void initSuperAdmin(){
        databaseReference = FirebaseDatabase.getInstance().getReference(pt.databases[0]).child(pt.databases[5]);
        User user = new SuperAdmin("super_admin", "superadmin", "superadmin@superadmin.it");
        databaseReference.setValue(user);
    }

    private void readData(){
        databaseUsers.child("Normali").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(DataSnapshot snapshot : dataSnapshot.getChildren()){
                    UserNormal userNormal = snapshot.getValue(UserNormal.class);
                    String email = userNormal.getEmail();
                    userNormalList.add(email);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void checkEmail(String email){
        if(userNormalList.contains(email)){
            startActivity(new Intent(LoginActivity.this, MainActivity.class));
        }else{
            startActivity(new Intent(LoginActivity.this, AdminActivity.class));
        }
    }
}