package com.app.oken.noleggiovideogiochireal;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.app.oken.extras.Game;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.NavigableMap;
import java.util.Timer;
import java.util.TimerTask;
import java.util.TreeMap;
import static com.app.oken.extras.PredefinedText.EXTRA_GENRE;
import static com.app.oken.extras.PredefinedText.EXTRA_TITOLO;
import static com.app.oken.extras.PredefinedText.EXTRA_URL;

public class NewsFragment extends Fragment{
    private int dotscount;
    private ImageView[] dots;
    ViewPager viewPager;
    LinearLayout sliderDotspanel;
    DatabaseReference databaseGames;
    String genre, date;
    public static String[] urls;
    public ImageAdapter imageAdapter;
    public static ArrayList<Game> gameList;
    private TreeMap<String, String> map;
    public NavigableMap<String, String> nmap;
    public static Context context;
    public static Fragment fragment;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View rootView = inflater.inflate(R.layout.fragment_news, container, false);

        databaseGames = FirebaseDatabase.getInstance().getReference("Giochi");
        viewPager = rootView.findViewById(R.id.viewPager);
        sliderDotspanel = rootView.findViewById(R.id.SliderDots);
        urls = new String[3];
        gameList = new ArrayList<>();
        map = new TreeMap<>();
        context = getContext();

        readData(new FirebaseCallback(){
            @Override
            public void onCallback(List<Game> gameList){
                map = iterList(gameList);
                nmap = map.descendingMap();

                int j = 0;
                for(Map.Entry<String, String> entry : nmap.entrySet()){
                    if(j == 3){
                        break;
                    }else{
                        String url = entry.getValue();
                        urls[j++] = url;
                    }
                }

                imageAdapter = new ImageAdapter(getActivity(), urls, gameList);
                viewPager.setAdapter(imageAdapter);
                dotscount = imageAdapter.getCount();

                dots = new ImageView[nmap.size()];

                for(int i = 0; i < dotscount; i++){
                    dots[i] = new ImageView(getActivity());
                    dots[i].setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_black_dot));
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,LinearLayout.LayoutParams.WRAP_CONTENT);
                    params.setMargins(8,0,8,0);
                    sliderDotspanel.addView(dots[i], params);
                    dots[0].setImageDrawable(ContextCompat.getDrawable(getActivity(),R.drawable.ic_grey_dot));
                    viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                        @Override
                        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels){
                            //
                        }

                        @Override
                        public void onPageSelected(int position){
                            for(int i = 0; i < dotscount; i++){
                                dots[i].setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_black_dot));
                            }
                            dots[position].setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_grey_dot));
                        }

                        @Override
                        public void onPageScrollStateChanged(int state){
                            //
                        }
                    });
                }
            }
        });
        
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new MyTimer(),2000,4000);
        return rootView;
    }

    public class MyTimer extends TimerTask{
        @Override
        public void run(){
            getActivity().runOnUiThread(new Runnable(){
                @Override
                public void run(){
                    if(viewPager.getCurrentItem() == 0){
                        viewPager.setCurrentItem(1);
                    }
                    else if(viewPager.getCurrentItem() == 1){
                        viewPager.setCurrentItem(2);
                    }
                    else{
                        viewPager.setCurrentItem(0);
                    }
                }
            });
        }
    }

    private void readData(final FirebaseCallback firebaseCallback){
        databaseGames.addValueEventListener(new ValueEventListener(){
            @Override
            public void onDataChange(DataSnapshot dataSnapshot){
                if(dataSnapshot.exists()){
                    for(DataSnapshot snapshot : dataSnapshot.getChildren()){
                        Game game = snapshot.getValue(Game.class);
                        gameList.add(game);
                    }
                    firebaseCallback.onCallback(gameList);
                }else{
                    Toast.makeText(getActivity(), "Nessun valore trovato", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError){
                Log.e("Error", databaseError.getMessage());
            }
        });
    }

    private interface FirebaseCallback{
        void onCallback(List<Game> gameList);
    }

    private TreeMap<String, String> iterList(List<Game> gameList){
        for(Game game : gameList){
            String date = game.getDate();
            String url = game.getUrl();
            map.put(date, url);
        }
        return map;
    }

    public static Game superFilter(String url){
        Game newGame = null;

        for(Game game : gameList){
            String tempTitolo = game.getTitolo();
            String tempGenere = game.getGenre();
            String tempUrl = game.getUrl();
            String tempDate = game.getDate();

            if(tempUrl.equals(url)){
                newGame = new Game(tempTitolo, tempGenere, tempUrl, tempDate);
            }
        }

        Intent intent = new Intent(context, DetailActivity.class);
        intent.putExtra(EXTRA_TITOLO, newGame.getTitolo());
        intent.putExtra(EXTRA_GENRE, newGame.getGenre());
        intent.putExtra(EXTRA_URL, newGame.getUrl());
        context.startActivity(intent);

        return newGame;
    }
}