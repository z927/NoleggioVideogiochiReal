package com.app.oken.noleggiovideogiochireal;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;
import com.app.oken.extras.Game;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import java.text.SimpleDateFormat;
import java.util.Date;
import static android.app.Activity.RESULT_OK;

public class GameFragment extends Fragment{
    Button btnAdd;
    DatabaseReference databaseGames, databaseValore;
    EditText txtTitolo;
    Spinner spinnerGenre;
    private ImageView imgCamera;
    final int ACTIVITY_SELECT_IMAGE = 1234;
    Uri imageUri;
    public DatabaseReference databaseReference;
    private static final String DATABASE_PATH = "Giochi";
    StorageReference storageReference;
    SimpleDateFormat dateFormat;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState){
        View rootView = inflater.inflate(R.layout.fragment_game, container, false);

        databaseGames = FirebaseDatabase.getInstance().getReference(DATABASE_PATH);
        databaseReference = FirebaseDatabase.getInstance().getReference(DATABASE_PATH);
        databaseValore = FirebaseDatabase.getInstance().getReference();
        storageReference = FirebaseStorage.getInstance().getReference();

        txtTitolo = rootView.findViewById(R.id.txtTitolo);
        spinnerGenre = rootView.findViewById(R.id.spinnerGenere);
        btnAdd = rootView.findViewById(R.id.btnAdd);

        btnAdd.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                checkAdd();
            }
        });

        imgCamera = rootView.findViewById(R.id.imgCamera);
        imgCamera.setClickable(true);
        imgCamera.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);
                startActivityForResult(i, ACTIVITY_SELECT_IMAGE);
            }
        });

        dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        return rootView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == RESULT_OK && requestCode == ACTIVITY_SELECT_IMAGE){
            imageUri = data.getData();
            imgCamera.setImageURI(imageUri);
        }
    }
    public void checkAdd(){
        final String titolo = txtTitolo.getText().toString().trim();
        final String genre = spinnerGenre.getSelectedItem().toString();

        if(!TextUtils.isEmpty(titolo) && (imageUri!= null)){
            final ProgressDialog progressDialog = new ProgressDialog(getActivity());
            progressDialog.setTitle("Caricamento immagine");
            progressDialog.setCancelable(false);
            progressDialog.show();

            StorageReference ref = storageReference.child("images/" + titolo);
            ref.putFile(imageUri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>(){
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot){
                            progressDialog.dismiss();
                            Toast.makeText(getActivity(), "Immagine caricata", Toast.LENGTH_SHORT).show();
                            Game game = new Game(titolo, genre, taskSnapshot.getDownloadUrl().toString(), dateFormat.format(new Date()).toString());
                            databaseGames.child(titolo).setValue(game);
                            Toast.makeText(getActivity(), "Il gioco è stato correttamente aggiunto nel DB", Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener(){
                        @Override
                        public void onFailure(@NonNull Exception e){
                            progressDialog.dismiss();
                            Toast.makeText(getActivity(), "Qualcosa è andato storto", Toast.LENGTH_SHORT).show();
                        }
                    }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>(){
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot){
                            double progress = (100.0 * taskSnapshot.getBytesTransferred() / taskSnapshot.getTotalByteCount());
                            progressDialog.setMessage("Caricamento: " + (int)progress + "%");
                        }
            });
        }else{
            Toast.makeText(getActivity(), "Si prega di inserire i dati correttamente", Toast.LENGTH_SHORT).show();
        }
    }
}