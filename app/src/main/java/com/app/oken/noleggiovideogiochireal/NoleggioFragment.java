package com.app.oken.noleggiovideogiochireal;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.app.oken.extras.Noleggio;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import java.util.ArrayList;

public class NoleggioFragment extends Fragment{
    private DatabaseReference databaseNoleggio;
    private ArrayList<Noleggio> noleggioList;
    private ListView listView;
    private ArrayAdapter<Noleggio> adapter;
    private TextView tvCosto;
    private double importo;
    private Button btnConferma;
    private CustomAdapter customAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState){
        View rootView = inflater.inflate(R.layout.fragment_noleggio, null);
        listView = rootView.findViewById(R.id.listaNoleggio);
        tvCosto = rootView.findViewById(R.id.tvCostoComplessivo);
        importo = 15.99;
        btnConferma = rootView.findViewById(R.id.btnConferma);
        btnConferma.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                confermaPagamento();
            }
        });

        tvCosto.setText("Costo Complessivo: " + importo + " €");
        databaseNoleggio = FirebaseDatabase.getInstance().getReference(DetailActivity.DATABASE_PATH + "/" + MainActivity.id);
        noleggioList = new ArrayList<>();
        riempiLista();
        removeItem();
        return rootView;
    }
    public void riempiLista(){
        databaseNoleggio.addValueEventListener(new ValueEventListener(){
            @Override
            public void onDataChange(DataSnapshot dataSnapshot){
                noleggioList.clear();
                if(dataSnapshot.exists()){
                    for(DataSnapshot snapshot : dataSnapshot.getChildren()){
                        Noleggio noleggio = snapshot.getValue(Noleggio.class);
                        noleggioList.add(noleggio);
                    }
                    adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, noleggioList);
                    customAdapter = new CustomAdapter(getActivity(), noleggioList);
                    listView.setAdapter(customAdapter);
                }else{
                    Toast.makeText(getActivity(), "Nessun valore trovato", Toast.LENGTH_SHORT).show();
                }
                calcola(importo, noleggioList.size());
                tvCosto.setText("Costo Complessivo: " + calcola(importo, noleggioList.size()) + " €");
            }
            @Override
            public void onCancelled(DatabaseError databaseError){
                databaseError.getMessage();
            }
        });
    }

    public void removeItem(){
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener(){
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, final int position, long l){
                removeElement(position);
                return true;
            }
        });
    }

    public double calcola(Double importo, int lunghezza){
        importo = importo * lunghezza;
        return importo;
    }
    public void confermaPagamento(){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setMessage("Vuoi procedere al pagamento?");
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton("Conferma", new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialogInterface, int i){
                startActivity(new Intent(getActivity(), MainActivity.class));
                Toast.makeText(getActivity(),"il pagamento è stato effettuato correttamente", Toast.LENGTH_SHORT).show();
            }
        });
        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialogInterface, int i){

            }
        });
        alertDialog.create().show();
    }
    class CustomAdapter extends BaseAdapter{
        ArrayList<Noleggio> noleggioList;
        Context mContext;

        public CustomAdapter(Context mContext, ArrayList<Noleggio> noleggioList){
            this.mContext = mContext;
            this.noleggioList = noleggioList;
        }
        @Override
        public int getCount(){
            return noleggioList.size();
        }
        @Override
        public Object getItem(int i){
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View view, ViewGroup viewGroup){
            view = getLayoutInflater().inflate(R.layout.custom_layout, null);
            Noleggio noleggio = noleggioList.get(position);

            TextView txtTitolo = view.findViewById(R.id.txtTitoloNol);
            TextView txtCosto = view.findViewById(R.id.txtCostoNol);
            TextView txtData = view.findViewById(R.id.txtDataNol);

            txtTitolo.setText("Titolo: " + noleggio.getTitolo());
            txtCosto.setText("Costo: " + noleggio.getCosto());
            txtData.setText("Data: " + noleggio.getDate());
            return view;
        }
    }

    private void removeElement(final int position){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setMessage("Sei sicuro di voler cancellare il noleggio relativo al seguente gioco?");
        alertDialog.setCancelable(false);
        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialogInterface, int i){

            }
        });
        alertDialog.setPositiveButton("Conferma", new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialogInterface, int i){
                Noleggio noleggio = noleggioList.remove(position);
                databaseNoleggio.child(noleggio.getTitolo()).removeValue();
                customAdapter.notifyDataSetChanged();
                Toast.makeText(getActivity(), "Il seguente elemento " + noleggio.getTitolo() + " è stato cancellato correttamente", Toast.LENGTH_LONG).show();
            }
        });
        alertDialog.create().show();
    }
}