package com.app.oken.noleggiovideogiochireal;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class AccountFragment extends Fragment{
    private String id, email;
    private TextView txtID, txtMail;
    private Button btnModifica, btnLogout;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState){
        View rootView = inflater.inflate(R.layout.fragment_account, null);
        id = getArguments().getString(MainActivity.EXTRA_ID);
        email = getArguments().getString(MainActivity.EXTRA_EMAIL);

        txtMail = rootView.findViewById(R.id.txtEmailM);

        txtMail.setText(email);

        btnModifica = rootView.findViewById(R.id.btnModifica);
        btnModifica.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){

            }
        });

        btnLogout = rootView.findViewById(R.id.btnLogout);
        btnLogout.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                Toast.makeText(getContext(), "Logging out..", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getActivity(), LoginActivity.class));
            }
        });

        Toast.makeText(getActivity(), id, Toast.LENGTH_SHORT).show();
        return rootView;
    }
}