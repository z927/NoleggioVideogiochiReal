package com.app.oken.noleggiovideogiochireal;

import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.oken.extras.Game;
import com.app.oken.extras.GameAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;


public class SearchFragment extends Fragment implements SearchView.OnQueryTextListener, GameAdapter.OnItemClickListener{
    private Toolbar toolbarSearch;
    private SearchView searchView;
    private TextView textView;
    private DatabaseReference databaseGames;
    private RecyclerView recyclerView;
    private List<Game> gameList;
    private ArrayList<Game> tempGame;
    private GameAdapter gameAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private Fragment fragment;
    private static final String DATABASE_PATH = "Giochi";
    public static final String EXTRA_URL = "imageURL";
    public static final String EXTRA_TITOLO = "titolo";
    public static final String EXTRA_GENRE = "genre";
    private int value=0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View rootView = inflater.inflate(R.layout.fragment_search, container, false);
        setHasOptionsMenu(true);

        //Firebase Database
        databaseGames = FirebaseDatabase.getInstance().getReference(DATABASE_PATH);

        //RecyclerView
        recyclerView = rootView.findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);

        //Variabili
        gameList = new ArrayList<>();
        return rootView;
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater){
        inflater.inflate(R.menu.search_menu, menu);
        MenuItem menuItem = menu.findItem(R.id.search_games);
        searchView = (SearchView) MenuItemCompat.getActionView(menuItem);
        searchView.setIconifiedByDefault(false);
        searchView.setQueryHint("Inserire gioco da cercare");
        searchView.setOnQueryTextListener(this);

        riempiRecyclerView();
        searchView.setOnCloseListener(new SearchView.OnCloseListener(){
            @Override
            public boolean onClose(){
                gameList.clear();
                return false;
            }
        });

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onQueryTextSubmit(String query){
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText){
        newText = newText.toLowerCase();

        if(newText.length() >= 1){
            value = 1;
        }else{
            value = 0;
        }
        tempGame = new ArrayList<>();

        for(Game game : gameList){
            String titolo = game.getTitolo().toLowerCase();
            if(titolo.contains(newText)){
                tempGame.add(game);
            }
        }
        gameAdapter.setFilter(tempGame);
        return true;
    }
    public void riempiRecyclerView(){
        databaseGames.addValueEventListener(new ValueEventListener(){
            @Override
            public void onDataChange(DataSnapshot dataSnapshot){
                for(DataSnapshot snapshot : dataSnapshot.getChildren()){
                    Game game = snapshot.getValue(Game.class);
                    gameList.add(game);
                }
                gameAdapter = new GameAdapter(getActivity(), gameList);
                recyclerView.setAdapter(gameAdapter);
                recyclerView.setLayoutManager(layoutManager);
                gameAdapter.setOnItemClickListener(SearchFragment.this);
            }

            @Override
            public void onCancelled(DatabaseError databaseError){
                Log.e("DB: ", databaseError.getDetails());
            }
        });
    }

    @Override
    public void onItemClick(int position){
        Bundle bundle = new Bundle();
        Game game;

        if(value == 0){
            game = gameList.get(position);
        }else{
            game = tempGame.get(position);
        }

        fragment = new DetailFragment();

        bundle.putString(EXTRA_TITOLO, game.getTitolo());
        bundle.putString(EXTRA_GENRE, game.getGenre());
        bundle.putString(EXTRA_URL, game.getUrl());

        fragment.setArguments(bundle);

        getFragmentManager().beginTransaction().replace(R.id.screen_area, fragment).commit();

    }
}