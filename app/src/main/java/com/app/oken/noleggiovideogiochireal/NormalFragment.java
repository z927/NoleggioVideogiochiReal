package com.app.oken.noleggiovideogiochireal;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.oken.extras.Noleggio;
import com.app.oken.extras.PredefinedText;
import com.app.oken.extras.User;
import com.app.oken.extras.UserAdmin;
import com.app.oken.extras.UserNormal;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;


public class NormalFragment extends Fragment{
    private DatabaseReference databaseReference, databaseAdmin;
    private PredefinedText pt;
    private List<UserNormal> userList;
    private ListView listView;
    private ArrayAdapter<UserNormal> adapter;
    private CustomAdapter customAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_normal, null);
        listView = rootView.findViewById(R.id.listNormal);
        pt = new PredefinedText();
        databaseReference = FirebaseDatabase.getInstance().getReference(pt.databases[0]);
        databaseAdmin = FirebaseDatabase.getInstance().getReference(pt.databases[0]);
        userList = new ArrayList<>();
        riempiLista();
        removeUser();
        return rootView;
    }
    public void riempiLista(){
        databaseReference.child(pt.databases[3]).addValueEventListener(new ValueEventListener(){
            @Override
            public void onDataChange(DataSnapshot dataSnapshot){
                userList.clear();
                if(dataSnapshot.exists()){
                    for(DataSnapshot snapshot : dataSnapshot.getChildren()){
                        UserNormal user = snapshot.getValue(UserNormal.class);
                        userList.add(user);
                    }
                    adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, userList);
                    customAdapter = new CustomAdapter(getActivity(), userList);
                    listView.setAdapter(customAdapter);
                }else{
                    Toast.makeText(getActivity(), "Nessun valore trovato", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError){
                databaseError.getMessage();
            }
        });
    }
    public void removeUser(){
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener(){
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, final int position, long l){
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setMessage("Vuoi promuovere il seguente utente ad Admin?");
                alertDialog.setCancelable(false);
                alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i){

                    }
                });
                alertDialog.setPositiveButton("Conferma", new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i){
                        UserNormal userNormal = userList.remove(position);
                        databaseAdmin.child(pt.databases[4]).child(userNormal.getNome()).setValue(userNormal);
                        databaseReference.child(pt.databases[3]).child(userNormal.getNome()).removeValue();
                        customAdapter.notifyDataSetChanged();
                        Toast.makeText(getActivity(), "Il seguente utente " + userNormal.getNome() + " è stato promosso ad admin", Toast.LENGTH_LONG).show();
                    }
                });
                alertDialog.create().show();
                return true;
            }
        });
    }
    class CustomAdapter extends BaseAdapter{
        List<UserNormal> userList;
        Context mContext;

        public CustomAdapter(Context mContext, List<UserNormal> userList){
            this.mContext = mContext;
            this.userList = userList;
        }
        @Override
        public int getCount(){
            return userList.size();
        }
        @Override
        public Object getItem(int i){
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View view, ViewGroup viewGroup){
            view = getLayoutInflater().inflate(R.layout.custom_user_normal_layout, null);
            UserNormal userNormal = userList.get(position);

            TextView txtId = view.findViewById(R.id.txtIdNormal);
            TextView txtMail = view.findViewById(R.id.txtMailNormal);

            txtId.setText("ID: " + userNormal.getNome());
            txtMail.setText("Email: " + userNormal.getEmail());
            return view;
        }
    }

}