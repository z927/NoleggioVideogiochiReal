package com.app.oken.noleggiovideogiochireal;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.app.oken.extras.PredefinedText;
import com.app.oken.extras.SuperAdmin;
import com.app.oken.extras.User;
import com.app.oken.extras.UserAdmin;
import com.app.oken.extras.UserNormal;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;

public class SignActivity extends AppCompatActivity{
    private Button btnSign;
    private EditText txtUserID, txtPassword, txtEmail;
    private FirebaseAuth auth;
    private DatabaseReference databaseReference;
    private ProgressDialog progressDialog;
    private String userID, email, password;
    private PredefinedText pt;
    private TextInputLayout tilUserId, tilPassword, tilMail;

    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign);

        pt = new PredefinedText();

        btnSign = (Button) findViewById(R.id.btnSign);
        btnSign.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                register();
            }
        });

        txtUserID = (EditText) findViewById(R.id.txtUserID);
        txtPassword = (EditText) findViewById(R.id.txtPassword);
        txtEmail = (EditText) findViewById(R.id.txtEmail);
        tilUserId =  findViewById(R.id.sign_input_userID);
        tilPassword = findViewById(R.id.sign_input_password);
        tilMail = findViewById(R.id.sign_input_email);
        auth = FirebaseAuth.getInstance();
        progressDialog = new ProgressDialog(this);

        databaseReference = FirebaseDatabase.getInstance().getReference("Utenti");
        databaseReference.child(pt.databases[5]).setValue(new SuperAdmin("SuperAdmin", "super_admin", "www.super-admin@gmail.com"));
    }

    public static boolean isValidEmail(CharSequence target){
        if(target == null){
            return false;
        }else{
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }
    public void register(){
        userID = txtUserID.getText().toString();
        email = txtEmail.getText().toString();
        password = txtPassword.getText().toString();

        if(TextUtils.isEmpty(userID)){
            tilUserId.setError("Inserire uno ID valido");
            return;
        }else if(TextUtils.isEmpty(email) || !(isValidEmail(email))){
            tilMail.setError("inserire una email valida");
            Toast.makeText(this, "Inserire una email valida", Toast.LENGTH_SHORT).show();
            return;
        }else if(TextUtils.isEmpty(password)) {
            tilPassword.setError("Inserire una password valida");
            return;
        }else{
            progressDialog.show();
            progressDialog.setMessage("Registrazione in corso");

            auth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>(){
                @Override
                public void onComplete(@NonNull Task<AuthResult> task){
                    if(task.isSuccessful()){
                            User utente = new UserNormal(userID, password, email);
                            FirebaseDatabase.getInstance().getReference(pt.databases[0]).child(pt.databases[3]).child(userID).setValue(utente).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task){
                                    if(task.isSuccessful()){
                                        Toast.makeText(getApplicationContext(), "Registrazione avvenuta correttamente", Toast.LENGTH_SHORT).show();
                                    }else{
                                        Toast.makeText(getApplicationContext(), "Credenziali Errate", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                            finish();
                            Toast.makeText(SignActivity.this, "Utente registrato correttamente", Toast.LENGTH_SHORT).show();
                    }else{
                        AlertDialog.Builder builder = new AlertDialog.Builder(SignActivity.this);
                        builder.setMessage(task.getException().getMessage()).setTitle("Errore").setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i){
                                dialogInterface.cancel();
                            }
                        });
                        AlertDialog dialog = builder.create();
                        dialog.show();
                    }
                }
            });
        }
    }
}